﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Input : MonoBehaviour
{
    [SerializeField]
    public InputField Lengte;
    [SerializeField]
    public InputField Breedte;
    [SerializeField]
    public Text Uitkomst;
    [SerializeField]
    public Text m_Standaard;
    [SerializeField]
    public Dropdown Dropdown1;
    [SerializeField]
    public List<Dropdown> Options;
    int m_L;
    int m_B;
    int m_U;
    int m_DropDown01 = 30;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Dropdown1.value == 0)
        {
            m_U = m_L * m_B * m_DropDown01;
        }

        int.TryParse(Lengte.text, out m_L);
        int.TryParse(Breedte.text, out m_B);    
        m_U = m_L * m_B;

        Uitkomst.text = m_U.ToString();
	}
}
